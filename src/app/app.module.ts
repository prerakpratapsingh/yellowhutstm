import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { ListComponent } from './menu/list/list.component';
import { AddComponent } from './menu/add/add.component';
import { EditComponent } from './menu/edit/edit.component';
import { LoginComponent } from './login/login.component';
import { GalleryComponent } from './gallery/gallery.component';
import { NavComponent } from './nav/nav.component';
import { LightboxComponent } from './lightbox/lightbox.component';
import { QueryformComponent } from './queryform/queryform.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    ListComponent,
    AddComponent,
    EditComponent,
    LoginComponent,
    GalleryComponent,
    NavComponent,
    LightboxComponent,
    QueryformComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
