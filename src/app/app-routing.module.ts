import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { ListComponent } from './menu/list/list.component';
import { AddComponent } from './menu/add/add.component';
import { EditComponent } from './menu/edit/edit.component';
import { LoginComponent } from './login/login.component';
import { GalleryComponent } from './gallery/gallery.component';
import { NavComponent } from './nav/nav.component';
import { LightboxComponent } from './lightbox/lightbox.component';
import { QueryformComponent } from './queryform/queryform.component';

const routes: Routes = [
{ path:'', redirectTo:'/home',pathMatch:'full'},
{path:'home',component:LandingPageComponent,data:{title:'Home Page'} },
{path:'login',component:LoginComponent,data:{title:'Login Page '}},
{path:'queryform',component:QueryformComponent,data:{title:'Query Form'}},
{path:'menu',component:ListComponent,data:{title:'Menu List'}}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
